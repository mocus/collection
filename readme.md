# Mox的收藏库——20220110

为什么码云和GitHub都没有收藏分类功能？
关注的欢迎Fork回去



[![Fork me on Gitee](https://gitee.com/mocus/collection/widgets/widget_6.svg)](https://gitee.com/mocus/collection)

## Magic Word
python -m http.Server 4444

npm config set registry https://registry.npm.taobao.org



## 各路浏览器隐私保护全览

https://privacytests.org/



## 安全类

https://www.sqlsec.com/tools.html



大佬的工具库  https://www.freebuf.com/author/Alpha_h4ck

大佬的免杀笔记 https://www.freebuf.com/articles/system/227461.html

[CVE漏洞的POC](https://gitee.com/mocus/collection/blob/master/CVEPOC.md)



### Arbitrium-RAT(木马)

https://github.com/BenChaliah/Arbitrium-RAT





### APKlab(vscode 的apk逆向)

https://github.com/Surendrajat/APKLab





### vulmap（民间的网络安全扫描）

https://github.com/zhzyker/vulmap



### Burpsuite

[http://wulitaotao.space/2020/12/18/burp-suite/](http://wulitaotao.space/2020/12/18/burp-suite/😃)

https://www.sqlsec.com/2020/10/winbp.html



### 基于外网的账号搜集器

https://github.com/qeeqbox/social-analyzer



### 一个免费的web应用防火墙
https://www.freebuf.com/articles/web/226418.html

https://github.com/qq4108863/hihttps/



### ShareWAF
http://www.sharewaf.com/



### 特殊软件
Zanti  幻影wifi 

[Fing Pro「网络工具」v11.5.2 for Android 解锁订阅专业版 —— 首屈一指的网络扫描工具，检测识别 WiFi 上所有设备 | 异星软件空间](https://www.yxssp.com/31320.html/comment-page-1)





### metasploit
https://github.com/rapid7/metasploit-framework/pull/12283

### BUG
https://github.com/sailay1996/UAC_bypass_windows_store

### K8tools
 https://github.com/k8gege/K8tools 

### Slowloris
https://github.com/gkbrk/slowloris

### PocSuite
https://www.freebuf.com/sectool/202210.html

### 学习资料

https://github.com/Wh0ale/SRC-experience

### OsslSigncode
https://github.com/mtrojnar/osslsigncode

### Badusb
https://github.com/PlatyPew/Digispark-Duckduino

https://github.com/mame82/duck2spark

https://github.com/Catboy96/Automator （Badusb生成器）

https://www.freebuf.com/articles/system/185293.html



[joelsernamoreno/badusb_examples](https://github.com/joelsernamoreno/badusb_examples) （比较齐全）



### LaZagne 

https://github.com/AlessandroZ/LaZagne (密码导出器)



## 日常类

https://github.com/xtaci/kcptun （KCP网络加速）

https://github.com/jackzhenguo/python-small-examples （Pyhton的小栗子）

https://github.com/hifocus/merger （二维码集合）

http://openresty.org/cn/ （均衡负载）

https://github.com/ruffrey/mailsac (邮箱)

https://github.com/denghongcai/forsaken-mail

https://51.ruyo.net/4068.html

https://51.ruyo.net/3210.html

https://www.linuxidc.com/index.htm（Linux学习）

https://github.com/geekxh/hello-algorithm （算法学习）

https://github.com/Metabolix/HackBGRT （修改UEFI图标）



https://gitlab.com/kalilinux/packages/kali-legacy-wallpapers  (Kali的壁纸)



**https://www.fosshub.com/**（**提供完全免费开源电脑软件的良心网站**）

**http://oeasy.org/**（你所有想到的都能学）

**https://pmgeek.net**（设计导航网站）



https://github.com/dbeaver/dbeaver  (用于连接各种数据库的免费客户端)





## AI类

###  PyTorch学习资源 

https://github.com/INTERMT/Awesome-PyTorch-Chinese

### 人脸检测模型

https://github.com/Linzaer/Ultra-Light-Fast-Generic-Face-Detector-1MB

### Spleeter（声音分离）

https://github.com/deezer/spleeter

### CG特效编程语言Taichi

https://github.com/yuanming-hu/taichi 

### AI消除文本马赛克

https://github.com/beurtschipper/Depix



## Gitbook

### 《面向程序员的数据挖掘指南》

https://github.com/yourtion/DataminingGuideBook

## Docker
https://www.freebuf.com/articles/container/236909.html （GVM扫描）

##VPS
（ucloud）
https://jike.info/topic/1897/ucloud-1%E6%A0%B82g%E4%BA%91%E6%9C%8D%E5%8A%A1%E5%99%A8%E4%BD%8E%E8%87%B388%E5%85%83-%E5%B9%B4-%E6%AF%94%E9%98%BF%E9%87%8C%E4%BA%91%E5%AD%A6%E7%94%9F%E6%9C%BA%E8%BF%98%E8%A6%81%E4%BE%BF%E5%AE%9C

https://webs.horain.net/



##奇怪的应用
http://www.iapps.me/

https://www.iconfinder.com/

## Putty

https://gofinall.com/62.html

## Ventoy
## 修改host解决steam的问题
https://www.dogfight360.com/blog/475/

## 3 种生成高强度密码的方法
https://blog.csdn.net/yychuyu/article/details/106576714

## ESNI解决方案
https://github.com/iyouport-org/relaybaton
https://github.com/SixGenInc/Noctilucent


##MSF类渗透工具
https://github.com/Screetsec/TheFatRat



## 键盘声音识别

https://github.com/ggerganov/kbd-audio



https://ggerganov.github.io/jekyll/update/2018/11/30/keytap-description-and-thoughts.html



Demo：

https://ggerganov.github.io/jekyll/update/2018/11/24/keytap.html



https://mp.weixin.qq.com/s/JVqE_lTn7xiB3W2oscNWVg







## 电脑算命

https://gitee.com/6tail



## 生成大量伪数据

https://github.com/Marak/faker.js



## 好用的Python

### Muscipy

使用代码写音乐，并未使用AI



### handcalcs

只能使用Jupyter

快速生成Latex

https://github.com/connorferster/handcalcs



## GitPython

https://mp.weixin.qq.com/s/N8qWMrzahE6qAJYCpil3tA





## 检测QQ是否被拉黑

https://github.com/tangzixiang0304/Shielded_detector

https://github.com/zerosoul/honeyed-words-generator





## 上班摸鱼

[用 WinUI 3 开发了一个摸鱼应用](https://github.com/DinoChan/Loaf[)





## Nginx配置文件生成器

https://www.digitalocean.com/community/tools/nginx

https://github.com/digitalocean/nginxconfig.io



