# Software

**Mark Text(Typora替代品)**



**act(本地化Github action)**







**BlueLotus_XSSReceiver**

接收XSS cookie



## Python

**Pyarmor**

Python加密工具



**httpx**

基于协程的requests



#### analog

https://github.com/Testzero-wz/analog/



## Nodejs

**Nativefier**

以最少的配置轻松地为任何网站创建桌面应用程序





## Go

### restic

服务器网站增量部分工具

https://github.com/restic/restic

https://juejin.cn/post/7014803100074672135#heading-19



### Minio

自行实现裸机对象存储COS

兼容S3规则

http://www.minio.org.cn/download.shtml#/linux





### DDNS-Go

简单好用的DDNS。自动更新域名解析到公网IP(支持阿里云、腾讯云dnspod、Cloudflare、华为云)    

https://github.com/jeessy2/ddns-go





## Java

### 短信转发器

https://github.com/pppscn/SmsForwarder



## Ruby

### SSl挂了（检测ssl证书是否过期）

https://github.com/firhq/sslguala-ce



## Emulator

Switch 模拟器

https://github.com/Ryujinx/Ryujinx





## CTF比赛使用

https://github.com/DasSecurity-HatLab/AoiAWD