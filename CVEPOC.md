## CVE漏洞观察
### MS15-034 (IIS)
https://www.cnblogs.com/peterpan0707007/p/8529261.html



### CVE-2016-3213（BadTunnel超级漏洞）

http://www.96sec.org/blog/post/200.html

http://www.vuln.cn/6076



### CVE-2017-7269 (IIS webDav)
https://blog.csdn.net/zzxx123520/article/details/79156637
### CVE-2017-11780
https://blog.csdn.net/zzxx123520/article/details/79156637

### CVE-2017-8464 （震网病毒）
https://github.com/nixawk/labs/tree/master/CVE-2017-8464

https://www.freebuf.com/news/143356.html

### CVE-2018-8174
https://github.com/0x09AL/CVE-2018-8174-msf

https://www.freebuf.com/vuls/224379.html



## 2019年



### CVE-2019-14287：sudo权限绕过漏洞
https://www.freebuf.com/vuls/217089.html
### meltdown漏洞
https://github.com/gkaindl/meltdown-poc

### Spectre漏洞
https://github.com/Eugnis/spectre-attack

### CVE-2019-0708
https://blog.csdn.net/weixin_44677409/article/details/94552370

https://github.com/n1xbyte/CVE-2019-0708

https://github.com/zerosum0x0/CVE-2019-0708



### CVE-2019-1388（windows本地提权）

https://mp.weixin.qq.com/s/mQqCuH6xOvYJC8-C0aRe4w





### CVE-2019-10999

Dlink摄像头漏洞

https://github.com/fuzzywalls/CVE-2019-10999

https://bbs.pediy.com/thread-271170.htm#msg_header_h2_0



### CVE-2019-15126（Wifi漏洞）
https://www.linuxidc.com/Linux/2020-02/162457.htm
https://www.welivesecurity.com/wp-content/uploads/2020/02/ESET_Kr00k.pdf



### CVE-2019-13272

Linux内核提权



## 2020年

### CVE-2020-0601
https://github.com/ollypwn/CVE-2020-0601

### CVE_2020-0022
https://github.com/leommxj/cve-2020-0022

### CVE-2020-8597(PPPD 远程代码执行漏洞)
https://www.anquanke.com/post/id/200390

### CVE-2020-1938（Tomcat 漏洞）
https://www.cnblogs.com/A66666/p/048ac3f5170d724b03f62531da252a20.html

https://github.com/mocusez/CNVD-2020-10487-Tomcat-ajp-POC

https://github.com/mocusez/CVE-2020-1938

https://blog.csdn.net/SouthWind0/article/details/105147369/

### CVE-2020-0796（微软win10 SMBghost 1903-1909）
https://www.icecliffs.cn/822.html

https://github.com/ZecOps/CVE-2020-0796-LPE-POC

https://github.com/cve-2020-0796/cve-2020-0796

https://mp.weixin.qq.com/s/vM4jPmklOmFTVo3xI-0JFg

https://github.com/chompie1337/SMBGhost_RCE_PoC
### CVE-2020-1967 （Openssl Dos）
https://cert.360.cn/warning/detail?id=83b4133611aba0131a5e18fb2ea46aba

### CVE-2020-1020/CVE-2020-0938 （微软字体漏洞）
http://hackernews.cc/archives/29936

https://mp.weixin.qq.com/s/RvTZWvcXiXsI7xB6L9RWIg

### CVE-2020-0096 （Strandhogg）
https://www.freebuf.com/news/237914.html

### CVE-2020-1206 （SMBbleed）
https://github.com/ZecOps/CVE-2020-1206-POC

https://blog.zecops.com/vulnerabilities/smbleedingghost-writeup-chaining-smbleed-cve-2020-1206-with-smbghost/

### CVE-2020-10713  (“ BootHole ”,Grub漏洞)
https://www.freebuf.com/news/245060.html



### CVE-2020-1350   （sigred 微软DNS漏洞）
https://www.freebuf.com/news/243542.html



### CVE-2020-16898  (Windows TCP/IP远程执行代码漏洞)

https://mp.weixin.qq.com/s/RTdkBbXWdzIBFpExZszQkg



### CVE-2020-27897：APPLE MACOS内核OOB写入权限提升漏洞

https://www.freebuf.com/vuls/258990.html



## 2021年

### CVE-2021-1675（PrintNightmare）

PrintNightmare LPE (PowerShell)

https://github.com/calebstewart/CVE-2021-1675





### CVE-2021-1732

Windows 提权

https://bbs.pediy.com/thread-271303.htm



### CVE-2021-3156

sudo堆栈溢出漏洞

https://blog.qualys.com/vulnerabilities-research/2021/01/26/cve-2021-3156-heap-based-buffer-overflow-in-sudo-baron-samedit



### CVE-2021-3115

Google修复了严重的Golang Windows RCE漏洞    

https://www.bleepingcomputer.com/news/security/google-fixes-severe-golang-windows-rce-vulnerability/



### **CVE-2021-3922**和**CVE-2021-3969**

影响所有默认安装在联想电脑上的Lenovo System Interface Foundation软件中低于 1.1.20.3版本的ImControllerService组件。



### **CVE-2021-4034**

pkexec本地提权漏洞复现



### CVE-2021-21972

Vmware

https://github.com/NS-Sp4ce/CVE-2021-21972



### CVE-2021-31956

https://bbs.pediy.com/thread-271140.htm

CVE-2021-31956是由Windows Ntfs组件系统存在整形溢出所导致，该漏洞可导致本地权限提升。





### CVE-2021-36934 

Windows提权漏洞复现

EXP 地址为：https://github.com/GossiTheDog/HiveNightmare

工具地址：https://github.com/SecureAuthCorp/impacket.git



### CVE-2021-42287与CVE-2021-42278

https://tttang.com/archive/1380/



### CVE-2021-44228

大名鼎鼎的log4j2漏洞

https://www.mocusez.site/posts/8a62.htm

https://github.com/mocusez/log4j-payload-generator





### CVE-2021-45388

KCodes NetUSB严重漏洞影响(Pwn)

https://bbs.pediy.com/thread-271152.htm

https://www.freebuf.com/articles/319650.html

https://thehackernews.com/2022/01/new-kcodes-netusb-bug-affect-millions.html

https://www.bleepingcomputer.com/news/security/kcodes-netusb-bug-exposes-millions-of-routers-to-rce-attacks/

https://threatpost.com/millions-routers-exposed-bug-usb-module-kcodes-netusb/177506/



## 2022年

### CVE-2022-21907

HTTP Protocol Stack Remote Code Execution Vulnerability.

https://github.com/antx-code/CVE-2022-21907